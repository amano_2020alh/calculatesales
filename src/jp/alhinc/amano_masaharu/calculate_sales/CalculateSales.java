package jp.alhinc.amano_masaharu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	private static final String UNEXPECTED_ERROR = "予期せぬエラーが発生しました";

	public static void main(String[] args) {
		// コマンドライン引数が存在するか確認
		if (args.length != 1) {
			System.out.println(UNEXPECTED_ERROR);
			return;
		}

		String branch = "支店";
		String commodity = "商品";

		// 支店定義ファイル
		String branchFileIn = "branch.lst";
		// 商品定義ファイル
		String commodityFileIn = "commodity.lst";

		// Map<支店コード、支店名>、Map<支店コード、売上額>
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		// Map<商品コード、商品名>、Map<商品コード、売上額>
		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

		// 定義ファイルの"コード"のフォーマット確認用の正規表現
		String regexBranch = "^(\\d){3}$";
		String regexCommodity = "[a-zA-Z0-9]{8}";

		// 定義ファイルの読み込み
		if(!inputFile(args[0], branchFileIn, branchNames, branchSales, regexBranch, branch)) {
			return;
		}
		if(!inputFile(args[0], commodityFileIn, commodityNames, commoditySales, regexCommodity, commodity)) {
			return;
		}

		// 売上ファイルの読み込み、集計
		File files[] = new File(args[0]).listFiles();

		// rcdファイルのList
		List<File> rcdFiles = new ArrayList<>();

		// 正規表現を用いて売上ファイルだけをリストに追加
		for (int i = 0; i < files.length; i++) {
			String saleFile = files[i].getName();

			// ディレクトリでなくファイルであることを確認
			if (saleFile.matches("^(\\d){8}\\.rcd$") && files[i].isFile()){
				rcdFiles.add(files[i]);
			}
		}

		// 売上ファイルの名前が連番になっているか確認
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;
		// 抽出した売り上げファイルを開き、Map branchSales/commoditySalesに値を格納
		for (File rcdFile: rcdFiles) {
			try {
				FileReader fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);

				// 売上ファイルの内容... "支店コード\n商品コード\n売上"  ex."002\nBQE00003\n100000"
				String line;
				List<String> lines = new ArrayList<>();
				while ((line = br.readLine()) != null) {
					lines.add(line);
				}

				String rcdName = rcdFile.getName();

				// 売上ファイルの内容が3行かどうか確認
				if (lines.size() != 3) {
					System.out.println(rcdName + "のフォーマットが不正です");
					return;
				}

				// 支店コード、商品コード
				String branchCode = lines.get(0);
				String commodityCode = lines.get(1);

				// 支店コードが不正でないか確認
				if (!branchNames.containsKey(branchCode)) {
					System.out.println(rcdName + "の支店コードが不正です");
					return;
				}

				// 商品コードが不正でないか確認
				if (!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdName + "の支店コードが不正です");
					return;
				}


				// 売上金額が数字であることを確認
				if (!lines.get(2).matches("^(\\d)+$")) {
					System.out.println(UNEXPECTED_ERROR);
					return;
				}

				// 売上をLong型に変換
				Long fileSale = Long.parseLong(lines.get(2));
				// 売上の累計を計算
				Long saleAmountBranch = branchSales.get(branchCode) + fileSale;
				Long saleAmountCommodity = commoditySales.get(commodityCode) + fileSale;

				// 売上金額の合計が10桁を超えないか確認
				for (Long saleAmount: new Long[] {saleAmountBranch, saleAmountCommodity}) {
					if (saleAmount >= 10000000000L) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}

				branchSales.put(branchCode, saleAmountBranch);
				commoditySales.put(commodityCode, saleAmountCommodity);

			}catch(IOException e) {
				System.out.println(UNEXPECTED_ERROR);
				return;

			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println(UNEXPECTED_ERROR);
						return;
					}
				}
			}
		}




		// 支店別/商品別集計ファイルの出力
		String branchFileOut = "branch.out";
		String commodityFileOut = "commodity.out";

		if (!outputSales(args[0], branchFileOut, branchNames, branchSales)) {
			return;
		}
		if (!outputSales(args[0], commodityFileOut, commodityNames, commoditySales)) {
			return;
		}

	}


	private static boolean inputFile(String filePath, String fileName
			, Map<String, String> nameMap, Map<String, Long> saleMap, String regex
			, String prefix) {

		BufferedReader br = null;

		// 定義ファイルの読み込み
		try {
			// 定義ファイルのファイルオブジェクト
			File file = new File(filePath, fileName);

			// 定義ファイルが存在するか確認
			if (!file.exists()) {
				System.out.println(prefix + "定義ファイルが存在しません");
				return false;
			}

			// ディレクトリでなくファイルであることを確認
			if (!file.isFile()) {
				System.out.println(UNEXPECTED_ERROR);
			}

			// ファイルリーダー、バッファリーダー
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			// "番号,名前"  ex."002,仙台支店","ABC00001,Windows"
			String line;
			while((line = br.readLine()) != null) {
				String[] names = line.split(",");

				// 定義ファイルのフォーマットが正しいか確認
				if (!(names[0].matches(regex) && names.length == 2)) {
					System.out.println(prefix + "定義ファイルのフォーマットが不正です");
					return false;
				}
					nameMap.put(names[0], names[1]);
					saleMap.put(names[0], 0L);
				}

		}catch(IOException e) {
			System.out.println(UNEXPECTED_ERROR);
			return false;

		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println(UNEXPECTED_ERROR);
					return false;
				}
			}
		}
		return true;
	}


	private static boolean outputSales(String filePath, String fileName
			, Map<String, String>nameMap, Map<String, Long>saleMap) {
		BufferedWriter bw = null;

		try {
			File file = new File(filePath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String key: nameMap.keySet()) {
				bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}

		}catch(IOException e) {
			System.out.println(UNEXPECTED_ERROR);
			return false;

		}finally {
			if (bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println(UNEXPECTED_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
